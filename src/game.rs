extern crate ansi_term;
extern crate json;
use ansi_term::{Colour, Style};
use enum_primitive::FromPrimitive;
use player::*;
use matrix::*;
use io::CommAgent;
use std::net::{UdpSocket};
use std::io::Error;
use std;

// Game instance types and impl

// Ticking game manager

pub struct Game {
    pub players: Vec<Player>,
    pub io: UdpSocket,
    pub tick_count: u32,
    pub canvas: Grid,
    pub comm: CommAgent,
}

#[macro_export]
macro_rules! at {
    ( $grid:expr, $x:expr, $y:expr ) => ($grid.canvas[$y as usize][$x as usize]);
    ( $grid:expr, $x:expr, $y:expr, $new:expr ) => ($grid.canvas[$y as usize][$x as usize] = $new);
}

impl Game {
    pub fn new(bindaddr: &str, connaddr: &str) -> Result<Self, Error> {
        let io = UdpSocket::bind(bindaddr)?;
        io.connect(connaddr)?;
        Ok(Game { players: vec![], io: io, tick_count: 0, canvas: [[Colour::Black; CANVAS_SIZE]; CANVAS_SIZE]})
    }

    // Functions migrated from manip::

    pub fn get_color_at(&self, coord: &Coord) -> Option<Colour> {
        coord.val_in(&self.canvas)
    }

    pub fn set_color_at(&mut self, coord: &Coord, new: &Colour) -> Option<()> {
        coord.set_val_in(&mut self.canvas, *new)
    }

    pub fn is_empty_at(&self, coord: &Coord) -> bool {
        self.get_color_at(coord)
            .map(|color| color == Colour::Black)
            .unwrap_or(true)
    }

    // Killer can be -1 for touching border
    pub fn send_kill(&self, killed: usize, killer: isize) -> Result<usize, Error> {
        self.io.send(json::stringify(object!{
            "action" => "kill",
            "killed" => killed,
            "killer" => killer
        }).as_bytes())
    }

    pub fn direction_to_vector(dir: &Direction) -> (i32, i32) {
        match dir {
            &Direction::UP => ((0, -1)),
            &Direction::RIGHT => ((1, 0)),
            &Direction::DOWN => ((0, 1)),
            &Direction::LEFT => ((-1, 0)),
        }
    }

    pub fn tick(&mut self) -> Result<(), &'static str> {
        // Move all players along their direction
        for player in &mut self.players {
            let newtail = player.cursor_pos.clone();
            player.cursor_pos.progress(&player.direction);
            player.tail.push(newtail);
        }

        // Kill players
        let mut to_kill = Vec::new();
        for (kindex, killer) in self.players.iter().enumerate() {
            for (index, killee) in self.players.iter().enumerate() {
                if killee.tail.contains(&killer.cursor_pos) {
                    try_res_msg!(self.send_kill(index, kindex as isize), "Failed to send kill JSON message to client");
                    to_kill.push(index);
                }
            }
            // killer may also have touched the border
            if killer.cursor_pos.valid().is_err() {
                try_res_msg!(self.send_kill(kindex, -1isize), "Failed to send kill JSON message to client");
                to_kill.push(kindex);
            }
        }
        for index in to_kill {
            self.players.remove(index);
        }

        self.tick_count += 1;
        Ok(())
    }

    pub fn recv(&self) -> Result<json::JsonValue, String> {
        let mut buffer = [0u8; 600];
        try_res_msg!(self.io.recv(&mut buffer), format!("Unable to receive from UDP socket"));

        match std::str::from_utf8(&buffer) {
            Ok(_str) => {
                let mut strvec: Vec<u8> = Vec::from(_str);
                strvec.retain(|&ch| ch != 0);
                let string = String::from_utf8(strvec).unwrap();
                Ok(try_res_msgerr!(json::parse(&string), "Unable to parse input JSON: "))
            },
            Err(_) => Err(format!("Unable to convert byte[] buffer to UTF-8 string"))
        }
    }

    // Ok(player index) or Err(error reason)
    pub fn recv_move(&mut self) -> Result<usize, String> {
        let parsed = self.recv()?;

        // Set the player's direction, which will apply on the next tick
        self.players[parsed["player_index"].as_usize().expect("Player_index not usize")]
            .direction = Direction::from_i32(parsed["direction"].as_i32().expect("Direction not i32")).expect("Direction unknown");
        
        Ok(0)
    }

    // returns the index of the player
    pub fn spawn_rand_player(&mut self) -> Option<usize> {
        let color_sample = rand_elem!(&COLORS).clone();
        match Player::new(color_sample, rand_elem!(&["The Honorable", "Bach", "Tronald"]).to_string(), &self.canvas) {
            Ok(player) => {
                self.players.push(player);
                Some(self.players.len() - 1)
            },
            Err(_) => None
        }
    }

    pub fn clear_canvas(&mut self) {
        for y in 0..CANVAS_SIZE {
            for x in 0..CANVAS_SIZE {
                self.canvas[x][y] = Colour::Black;
            }
        }
    }

    pub fn render_all(&mut self) {
        self.clear_canvas();       

        for player in &self.players {
            player.render(&mut self.canvas);
        }
    }

    pub fn print_canvas_view(&self, width: usize, height: usize, center: &Coord) -> Option<()> {
        let ltop = center.new_add((width - 1) as i32 / -2, (height - 1) as i32 / -2);
        let rbottom = center.new_add((width - 1) as i32 / 2, (height - 1) as i32 / 2);

        if ltop.valid().is_err() || rbottom.valid().is_err() {
            return None;
        }

        for y in ltop.y..(rbottom.y + 1) {
            for x in ltop.x..(rbottom.x+1) {
                print!("{}", Style::new().on(at!(self, x, y)).paint(" "));
            }
            print!("\n");
        }
        // Reset cursor position
        print!("\x1b[{}A", rbottom.y - ltop.y + 1);
        Some(())
    }

    pub fn print_canvas(&self) {
        self.print_canvas_view(CANVAS_SIZE - 1, CANVAS_SIZE - 1, &coord!(CANVAS_SIZE/2, CANVAS_SIZE/2));
    }
}