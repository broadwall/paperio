// Backend to frontend communication agent (via WebSocket)

use json::JsonValue;
use json::{parse, stringify};
use std::collections::HashMap;
use std::thread;
use std::sync::RwLock;
use std::net::{TcpListener, TcpStream};
use tungstenite::{accept, Error, WebSocket};

pub struct Handler {
    thread: thread::JoinHandle<()>,
    socket: RwLock<WebSocket<TcpStream>>,
}

macro_rules! msg_onerr {
    ( $trydo:expr, $errmsg:expr ) => (match $trydo {
        Ok(item) => item,
        Err(error) => {
            println!("{}{:?}", $errmsg, error);
            continue;
        }
    })
}

impl Handler {
    pub fn new(stream: TcpStream, main_queue: RwLock<Vec<JsonValue>>) -> Result<Self, String> {
        let socket = RwLock::new(try_res_msgerr!(accept(stream),
            "Could not accept Websocket handshake: "));

        Ok(Handler {
            thread: try_res_msgerr!(thread::Builder::new()
                .name(format!("Handler for {:?}", stream))
                .spawn(move || {
                    loop {
                        {
                            let sock = socket.write().expect("Could not obtain WebSocket RwLock");
                            if let Ok(message) = sock.read_message() {
                                
                                let text_res = msg_onerr!(message.to_text(),
                                    "sock.read_message() received binary data, ignoring: ");
                                let object = msg_onerr!(parse(text_res),
                                    "Received message invalid: ");

                                main_queue.write().expect("Could not obtain main queue RwLock").push(object);
                                
                            } else {
                                println!("sock.read_message() failed, returning");
                                break;
                            }
                        } // This closure drops the RwLock to the socket, which allows the main thread to broadcast messages
                        // 10 loops per second, or 100 milliseconds between each loop
                        thread::sleep_ms(100);
                    }
                }), "Could not spawn thread for Handler: "),
            socket: socket,
        })
    }
}