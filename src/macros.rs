// Convenience macros

#[macro_use]
pub mod mac {
    #[macro_export]
    macro_rules! foreach_cell {
        ( $todo:tt ) => ({
            for y in 0..CANVAS_SIZE {
                for x in 0..CANVAS_SIZE {
                    $todo
                }
            }
        })
    }

    #[macro_export]
    macro_rules! try_res_msg {
        ( $x:expr, $msg:expr ) => (match $x {
            Ok(i) => i,
            Err(_) => return Err($msg)
        })
    }

    #[macro_export]
    macro_rules! try_res_msgerr {
        ( $x:expr, $msg:expr ) => (match $x {
            Ok(i) => i,
            Err(error) => return Err(format!("{}{:?}", $msg, error))
        })
    }

    #[macro_export]
    macro_rules! rand_elem {
        ( $iter:expr ) => ({
            use ::rand::*;
            &$iter[thread_rng().gen_range(0, $iter.len())]
        })
    }   
}