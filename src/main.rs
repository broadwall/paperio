extern crate ansi_term;
#[macro_use]
extern crate json;
extern crate termsize;
extern crate rand;
#[macro_use]
extern crate enum_primitive;
extern crate num;
extern crate ws;
#[macro_use] mod macros;
#[macro_use] pub mod matrix;
pub mod game;
pub mod player;
pub mod io;
use game::Game;

fn main() {
    let mut game = Game::new("127.0.0.1:25554", "127.0.0.1:29887").expect("Game IO address fault");
    for _ in 0..5 {
        game.spawn_rand_player().expect("Unable to spawn rand player");
    }

    loop {
        game.render_all();
        game.print_canvas();
        game.tick().expect("Game tick failed");
        game.recv_move().expect("Move recv failed");
    }
}