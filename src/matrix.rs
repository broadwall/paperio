extern crate ansi_term;
use ansi_term::Colour;
use game::Game;

// Matrix manipulation and types

#[derive(Debug)]
pub struct Coord {
    pub x: i32,
    pub y: i32,
}

impl Coord {
    pub fn add_x(&mut self, addend: i32) {
        self.x += addend;
    }
    pub fn add_y(&mut self, addend: i32) {
        self.y += addend;
    }
    pub fn add(&mut self, x: i32, y: i32) {
        self.add_x(x);
        self.add_y(y);
    }

    pub fn new_add(&self, x: i32, y: i32) -> Self {
        Coord { x: self.x + x, y: self.y + y }
    }

    // Error type is 'static because they must be hard-coded
    pub fn valid(&self) -> Result<(), &'static str> {
        if self.x < 0 || self.y < 0 {
            return Err("Negative x or y value")
        }
        if self.x as usize >= CANVAS_SIZE {
            return Err("X value not in bounds")
        }
        if self.y as usize >= CANVAS_SIZE {
            return Err("Y value not in bounds")
        }
        Ok(())
    }

    pub fn progress(&mut self, dir: &Direction) {
        let (x, y) = Game::direction_to_vector(dir);
        self.add(x, y);
    }

    pub fn val_in(&self, grid: &Grid) -> Option<Colour> {
        if self.valid().is_err() {
            return None;
        }
        Some(grid[self.y as usize][self.x as usize])
    }
    
    pub fn set_val_in(&self, grid: &mut Grid, new: Colour) -> Option<()> {
        if self.valid().is_err() {
            return None;
        }
        grid[self.y as usize][self.x as usize] = new;
        Some(())
    }
}

impl Clone for Coord {
    fn clone(&self) -> Self {
        Coord { x: self.x, y: self.y }
    }
}

impl PartialEq for Coord {
    fn eq(&self, other: &Coord) -> bool {
        self.x == other.x && self.y == other.y
    }
}

macro_rules! coord {
    ( $x:expr, $y:expr ) => (Coord { x: $x as i32, y: $y as i32 })
}

// Block of coords

#[derive(Debug, Clone)]
pub struct Block {
    pub left_top: Coord,
    pub right_bottom: Coord,
}

impl Block {
    // Specify left top and side length to generate the right bottom coordinate and instantiate Block (if rbottom and ltop are valid)
    pub fn new_sidelen(ltop: Coord, sidelen: u32) -> Option<Self> {
        let rbottom = ltop.new_add(sidelen as i32, sidelen as i32);

        if ltop.valid().is_err() || rbottom.valid().is_err() {
            None
        } else {
            Some(Block::new(ltop, rbottom))
        }
    }

    pub fn new(ltop: Coord, rbottom: Coord) -> Self {
        Block { left_top: ltop, right_bottom: rbottom }
    }

    // Aborts, returning false, when the function returns false
    // If all function return values are true, returns true
    pub fn foreach_coord<F>(&self, exec: F) -> bool 
        where F: Fn(Coord) -> bool {
        for y in self.left_top.y..(self.right_bottom.y + 1) {
            for x in self.left_top.x..(self.right_bottom.x + 1) {
                if !exec(Coord { x: x, y: y }) {
                    return false;
                }
            }
        }
        true
    }

    pub fn empty_in(&self, grid: &Grid) -> bool {
        self.foreach_coord(|coord| grid[coord.y as usize][coord.x as usize] == Colour::Black)
    }

    pub fn all_coords(&self) -> Vec<Coord> {
        let mut out = Vec::new();
        for y in self.left_top.y..(self.right_bottom.y + 1) {
            for x in self.left_top.x..(self.right_bottom.x + 1) {
                out.push(coord!(x, y));
            }
        }
        out
    }
    
    pub fn sidelen(&self) -> u32 {
        self.right_bottom.x as u32 - self.left_top.x as u32
    }
    
    pub fn center_coord(&self) -> Coord {
        let slen = self.sidelen();
        self.left_top.new_add((slen - 1) as i32 / 2, (slen - 1) as i32 / 2)
    }
    
    pub fn shrink(&mut self, new_sidelen: u32) -> Option<()> {
        let slen = self.sidelen();
        
        if slen == new_sidelen {
            Some(())
        } else if slen < new_sidelen {
            None
        } else {
            // Flip-flop solution
            let mut flip = false;
            while self.sidelen() != new_sidelen {
                if flip {
                    self.right_bottom.add(-1, -1);
                } else {
                    self.left_top.add(1, 1);
                }
                flip = !flip;
            }
            Some(())
        }
    }
}

// Direction / Vector / Velocity

enum_from_primitive! {
#[derive(Debug)]
pub enum Direction {
    UP = 0,
    RIGHT = 1,
    DOWN = 2,
    LEFT = 3,
}}

// Allocate the game space

pub const CANVAS_SIZE: usize = 60;
// Note that all access are canvas[y][x]
pub type Grid = [[Colour; CANVAS_SIZE]; CANVAS_SIZE];
