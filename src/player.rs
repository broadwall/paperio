use matrix::*;
use ansi_term::{Colour};
use ws::{Sender, Message};

// Player types and impls

/*
 * SECTION: PLAYER COLORS
 */

#[derive(Copy, Clone, Debug)]
pub struct PlayerColor {
    pub territory: Colour,
    pub cursor: Colour,
    pub tail: Colour
}

// COLORS follows Material Design;
// territory: 800,
// cursor: 900,
// tail: 600
pub const COLORS: [PlayerColor; 5] = [PlayerColor {
    // Purple
    territory: Colour::RGB(106, 27, 154),
    cursor: Colour::RGB(74, 20, 140),
    tail: Colour::RGB(142, 36, 170),
}, PlayerColor {
    // Green
    territory: Colour::RGB(46, 125, 50),
    cursor: Colour::RGB(27, 94, 32),
    tail: Colour::RGB(67, 160, 71),
}, PlayerColor {
    // Deep Orange
    territory: Colour::RGB(216, 67, 21),
    cursor: Colour::RGB(191, 54, 12),
    tail: Colour::RGB(244, 81, 30),
}, PlayerColor {
    // Red
    territory: Colour::RGB(198, 40, 40),
    cursor: Colour::RGB(183, 28, 28),
    tail: Colour::RGB(229, 57, 53),
}, PlayerColor {
    // Yellow
    territory: Colour::RGB(249, 168, 37),
    cursor: Colour::RGB(245, 127, 23),
    tail: Colour::RGB(253, 216, 53),
}];




// Player class definition

pub struct Player {
    pub color: PlayerColor,
    pub nickname: String,
    pub owned_blocks: Vec<Coord>,
    pub tail: Vec<Coord>,
    pub cursor_pos: Coord,
    pub direction: Direction,
    pub sender: Sender,
}

// TODO: Finalized solution: integrate WebSocket handling into Player, with WS-RS async event handlers
impl Player {
    pub fn new(color: PlayerColor, nick: String, location: &Grid) -> Result<Self, String> {
        // if is_user then needs to be spawned in view
        // Criterion for spawning is 5x5 empty space

        let mut empty_blocks: Vec<Block> = Vec::new();
        for y in 0..(CANVAS_SIZE - 5) {
            for x in 0..(CANVAS_SIZE - 5) {
                let ltop = coord!(x, y);
                if let Some(block) = Block::new_sidelen(ltop, 5) {
                    if block.empty_in(location) {
                        empty_blocks.push(block);
                    }
                }
            }
        }
        if empty_blocks.is_empty() {
            return Err(format!("Not enough space to spawn new player"));
        }
        // TODO: refactor to comply with Block

        let mut owned_block: Block = rand_elem!(empty_blocks).clone();
        owned_block.shrink(3);

        Ok(Player {
            color: color,
            nickname: nick,
            owned_blocks: owned_block.all_coords(),
            tail: vec![],
            cursor_pos: owned_block.center_coord(),
            direction: Direction::UP })
    }

    pub fn render(&self, grid: &mut Grid) {
        for coord in &self.owned_blocks {
            coord.set_val_in(grid, self.color.territory);
        }
        for tailc in &self.tail {
            tailc.set_val_in(grid, self.color.tail);
        }
        self.cursor_pos.set_val_in(grid, self.color.cursor);
    }
}